﻿using System;
using System.Collections.Generic;

namespace Snblog.Models
{
    public partial class SnVideoType
    {
        public int VId { get; set; }
        public string VType { get; set; }
    }
}
